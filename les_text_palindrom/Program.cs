﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace les_text_palindrom
{
    class Program
    {
        static void Main(string[] args)
        {
            string s;
            using (StreamReader sr = new StreamReader("input.txt"))
            {
                s = sr.ReadToEnd();
            }
            string[] words = s.Split(' ');
            for (int i = 0; i<words.Length; i++)
            {
                string w = words[i];
                bool ispal = true;
                for (int j = 0; j<=w.Length/2; j++)
                {
                    Console.WriteLine(j);
                    if (w[j] != w[w.Length-j-1]) { ispal = false; }
                }
                if (ispal == false) { Console.WriteLine(words[i]); words[i] = ""; }
            }
            BubbleSort(words);
            int maxl = words[words.Length - 1].Length;
            Console.ReadKey();
        }
        static void BubbleSort(string[] mas)
        {
            string temp;
            for (int i = 0; i < mas.Length; i++)
            {
                for (int j = i + 1; j < mas.Length; j++)
                {
                    if (mas[i].Length > mas[j].Length)
                    {
                        temp = mas[i];
                        mas[i] = mas[j];
                        mas[j] = temp;
                    }
                }
            }
        }
    }
}
